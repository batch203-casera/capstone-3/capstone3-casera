import React, { useEffect, useState } from 'react'

export default function Admin() {
    const [products, setProducts] = useState([]);
    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/product/listAll`)
            .then(response => response.json())
            .then(data => setProducts(data.data));
    }, []);

    return (
        <div>Admin</div>
    )
}