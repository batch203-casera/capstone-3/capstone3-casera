import React, { useEffect } from 'react'
import { Navigate } from 'react-router-dom';
import { useContext } from 'react';
import userContext from '../contexts/userContext';
import Swal from "sweetalert2";

export default function Logout() {
  const { unsetUser, setUser } = useContext(userContext);
  Swal.fire({
    title: "Logout Successful",
    iconHtml: `<img src="https://media4.giphy.com/media/LMQo5ad3JdjCAba7Sv/giphy.gif?cid=ecf05e474hfa5to7sisznvdb4uf409yt9uxvtxajxqwrbyqf&rid=giphy.gif&ct=g" alt="Close button" width="150px"/>`,
    customClass: {
      icon: 'no-border'
    },
    text: "You can revive back by logging in!"
  })
  unsetUser();
  useEffect(() => {
    return () => {
      setUser({ id: null, isAdmin: null });
    }
  }, [])
  return (
    <Navigate to="/" />
  )
}
